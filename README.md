# README #

This is a 2D game developed using Java Swing.

### Setup Instructions ###
* Navigate to the file (aptly) named: 'Launchgame.java', which is located at: '/src/com/spacecraft/Launchgame.java'.
* This is the main class, which can be used to run the application.

#### Note ####
* Make sure you have Java installed.